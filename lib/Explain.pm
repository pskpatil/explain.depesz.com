package Explain;

use Mojo::Base 'Mojolicious';

sub startup {
    my $self = shift;

    $self->sessions->cookie_name( 'explain' );
    $self->sessions->default_expiration( 60 * 60 * 24 * 365 );

    # Add path to pgFormatter library, from git submodule
    push @INC, $self->home->rel_file( 'ext/pgFormatter/lib' )->to_string();

    # register Explain plugins namespace
    $self->plugins->namespaces( [ "Explain::Plugin", @{ $self->plugins->namespaces } ] );

    # load configuration
    my $config = $self->plugin( 'JSONConfig' );

    # setup secret passphrase - later versions of
    # mojolicious require secrets to be multiple in an
    # array format
    my $use_secret = $config->{ secret } || 'Xwyfe-_d:yGDr+p][Vs7Kk+e3mmP=c_|s7hvExF=b|4r4^gO|';
    if ( $self->can( 'secrets' ) ) {

        # We're on Mojolicious 4.63 or newer
        $self->secrets( [ $use_secret ] );
    }
    else {
        # We're on old Mojolicious
        $self->secret( $use_secret );
    }

    # startup database connection
    $self->plugin( 'database', $config->{ database } || {} );

    # startup mail sender
    $self->plugin( 'mail_sender', $config->{ mail_sender } || {} );

    # load number_format plugin
    $self->plugin( 'number_format' );

    # Plugin to check if static file exists
    $self->plugin( 'Explain::Plugin::StaticExists' );

    # routes
    my $routes = $self->routes;

    # route: 'index'
    $routes->any( '/' )->to( 'controller#index' )->name( 'new-explain' );

    # route: 'status'
    $routes->any( '/status' )->to( 'controller#status' )->name( 'status' );

    # route: 'new-optimization'
    $routes->any( '/new_optimization' )->to( 'controller#new_optimization' )->name( 'new-optimization' );

    # route: 'user-history'
    $routes->any( '/user-history/:direction/:key' )->to( 'controller#user_history', direction => undef, key => undef )->name( 'user-history' );

    # route: 'plan-change'
    $routes->any( '/plan-change/:id' )->to( 'controller#plan_change' )->name( 'plan-change' );

    # route: 'login'
    $routes->any( '/login' )->to( 'controller#login' )->name( 'login' );

    # route: 'logout'
    $routes->any( '/logout' )->to( 'controller#logout' )->name( 'logout' );

    # route: 'user'
    $routes->any( '/user' )->to( 'controller#user' )->name( 'user' );

    # route: 'show'
    $routes->any( '/s/:id' )->to( 'controller#show', id => '' )->name( 'show' );

    # route: 'iframe'
    $routes->any( '/i/:id' )->to( 'controller#show', id => '' )->name( 'iframe' );

    # route: 'delete'
    $routes->any( '/d/:id/:key' )->to( 'controller#delete', id => '', key => '' )->name( 'delete' );

    # route: 'history'
    $routes->any( '/history/:date' )->to( 'controller#history', date => '' )->name( 'history' );

    # route: 'contact'
    $routes->get( '/contact' )->to( 'controller#contact' )->name( 'contact' );
    $routes->post( '/contact' )->to( 'controller#contact_post' )->name( 'contact_post' );

    # route: 'help'
    $routes->any( '/help' )->to( 'controller#help' )->name( 'help' );

    # route: 'info'
    $routes->any( '/info' )->to( 'controller#info' )->name( 'info' );

    return;
}

1;
